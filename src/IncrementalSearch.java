import java.util.Arrays;
import java.util.Scanner;

public class IncrementalSearch {

    public static final int MIN = -25;
    public static final int MAX = 51;
    public static final int ONE = 1;

    public static void main(String[] args) {
        int numberCells=inputNumberСells();
        int[] array = new int[numberCells];
        randomNumber(array);
        System.out.println(Arrays.toString(array));
        int number=inputNumber();
        int search = search(array, number);
        print(search);
    }

    public static int inputNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите искомое число: ");
        int number = scanner.nextInt();
        return number;
    }

    public static int inputNumberСells() {
        Scanner scanner;
        scanner = new Scanner(System.in);
        System.out.println("Введите кол-во ячеек: ");
        int number =scanner.nextInt();
        return number;
    }

    public static void randomNumber(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = MIN + (int) (Math.random() * MAX);
        }
    }

    public static int search(int[] array, int number) {
        int index = -1;
        for (int i = 0; i <array.length; i++) {
            if (array[i] == number) {
                index = i;
                break;
            }
        }
        return index;
    }

    public static void print(int search) {
        if (search == -1) {
            System.out.println("нет такого значения");
        } else {
            System.out.println("номер элемента в последовательности " + (search + ONE));
        }
    }
}
