package refactoringV2;

import java.util.Arrays;
import java.util.Scanner;

public class IncrementalSearch {

    public static final int MIN = -25;
    public static final int MAX = 51;

    /**
     * Класс для поиска индекса числа в массиве
     * @param args
     */

    public static void main(String[] args) {
        int[] array = new int[input("Введите кол-во ячеек массива: ")];
        completion(array);
        System.out.println(Arrays.toString(array));
        print(search(array, input("Введите искомое число: ")));
    }

    /**
     * Возвращает целое число, введенное пользователем
     * @param message выводимое сообщение пользователю
     * @return возращается число введеное пользователем
     */

    public static int input(String message) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(message);
        return scanner.nextInt();
    }

    /**
     *Заполняет массив числами от -25 до 25
     * @param array пустой массив
     */

    public static void completion(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = MIN + (int) (Math.random() * MAX);
        }
    }

    /**
     *Ищет число введеное пользователю
     * @param array заполненный массив
     * @param number число пользователя
     * @return индекс числа пользователя
     */

    public static int search(int[] array, int number) {
        int index = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                index = i;
                break;
            }
        }
        return index;
    }

    /**
     *Завершающее сообщение пользователю с найденным или не найденным числом
     * @param index индекс числа пользователя
     */

    public static void print(int index) {
        if (index == -1) {
            System.out.println("нет такого значения");
        } else {
            System.out.println("номер элемента в последовательности: " + (index + 1));
        }
    }
}

